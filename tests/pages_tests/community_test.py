from app_main import app
from tests import OhmTestCase
from pages import community

class CommunityTest(OhmTestCase):
    def test_get(self):
        with app.test_client() as c:
            response = c.get('/community')
            assert "Elvis Presley" in response.data

    def test_modal_content(self):
        with app.test_client() as c:
            response = c.get('/community/_modal.html?id=1')
            assert 'Chuck Norris is from EUROPE' in response.data


    def test_query(self):
        users = community._get_recents()
        assert len(users) == 3
        justin, elvis, chuck = users
        assert len(justin) == 5
        uid, name, tier, points, phone_query = justin
        assert uid == 3
        assert name == 'Justin Bieber'
        count = 0
        for _ in phone_query:  # check that no phone numbers are recalled.
            count += 1
        assert count == 0
        uid, name, tier, points, phone_query = chuck

        assert uid == 1
        count = 0
        for _ in phone_query:
            count += 1
        assert count == 2

