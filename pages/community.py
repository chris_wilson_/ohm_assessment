from flask import jsonify, render_template, request, Response
from flask.ext.login import current_user, login_user
from sqlalchemy.sql import text

from functions import app
import models


@app.route('/community', methods=['GET'])
def community():
    """
    Displays a table with the 5 most recently added users, using the user_id column value as a proxy
    for addition order - MySQL iterates this automatically with every user addition..

    """

    login_user(models.User.query.get(1))

    user_info = _get_recents()

    args = {
        'recent_users': user_info,
    }
    return render_template("community.html", **args)


def _get_recents():
    """
    This uses SQL injection for speed and unpacks them to Python objects. The first query gets recent items
    from the user table, and the second query uses the retrieved user ids to query the phone numbers. Use
    of ORM was avoided to make things a bit faster.

    It is also worth considering implementation of a cache for the most most recent user ids, i.e.
    Dogpile that would be invalidated on addition of a new user elsewhere in the app.

    Note that the values from the user table are unpacked, but the phone numbers are returned as a query.

    :return List of user information [uid, display name, tier, point_balance, phone_query]
    """

    users_statement = '''SELECT user.user_id, user.display_name, user.tier, user.point_balance
                    FROM user 
                    ORDER BY user_id DESC LIMIT 5;'''

    users_query = models.db.session.execute(users_statement)

    phone_statement = text('''SELECT rel_user_multi.attribute 
                             FROM rel_user_multi 
                             WHERE rel_user_multi.user_id = :x AND rel_user_multi.rel_lookup = 'PHONE';''')

    user_info = []
    for user in users_query:
        # I'm unpacking here because we need to get the user id and don't want to query twice.
        # This list is only max length of 5, so I'm not
        user_unpacked = [x for x in user]
        uid = user_unpacked[0]
        phone_query = models.db.session.execute(phone_statement, {'x': uid})
        user_unpacked.append(phone_query)
        user_info.append(user_unpacked)
    return user_info




@app.route('/community/_modal.html', methods=['GET'])
def community_modal():
    """
    This generates an HTML stub for the user modal.

    Retrieves user location information and generates template for modal display in community page.

    Handles edge case if reluser is not present.
    """

    uid = request.args.get('id')
    user = models.User.query.get(uid)
    reluser = user.get_rel_user_attribute('LOCATION')
    if reluser:
        reluser = reluser.attribute
    else:
        reluser = 'unknown'

    args = {
        'user': models.User.query.get(uid),
        'location': reluser
    }
    return render_template("community_modal.html", **args)
