"""Modify user 1 points, user 3 tier, and add user 2 entry to rel_user table.

Revision ID: 17a7884f2cac
Revises: 00000000
Create Date: 2018-04-26 10:19:58.539488

"""

# revision identifiers, used by Alembic.
revision = '17a7884f2cac'
down_revision = '00000000'

from alembic import op
# import sqlalchemy as sa
import models


def upgrade():

    # Using ORM just because I want to get a handle on the models.
    user_1 = models.User.query.get(1)  # type: models.User
    user_1.add_points(1000)

    user_3 = models.User.query.get(3)  # type: models.User
    user_3.set_tier('Silver')

    op.execute('''INSERT INTO rel_user (user_id, rel_lookup, attribute)
        VALUES 
          (2, 'LOCATION', 'USA')
    ''')




def downgrade():

    user_1 = models.User.query.get(1)  # type: models.User
    user_1.add_points(-1000)
    user_3 = models.User.query.get(3)  # type: models.User
    user_3.set_tier('Carbon')

    op.execute('''DELETE FROM rel_user WHERE  user_id = 2''')


